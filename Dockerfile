FROM sitapati/docker-alpine-python-node AS builder
WORKDIR /usr/src/app
RUN apk add --no-cache --update nodejs nodejs-npm
COPY package.json ./
RUN npm install --production
RUN npm install express

FROM sitapati/docker-alpine-python-node
RUN apk add --no-cache --update nodejs
COPY --from=builder /usr/src/app/node_modules ./node_modules
COPY dist dist
EXPOSE 4000
ENTRYPOINT ["node","dist/server"]
