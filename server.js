const path = require('path');
const express = require('express');
const app = express();

// Run the app by serving the static files
// in the dist directoryheroku
app.use( express.static(path.join(__dirname, '/dist/qq-client-app')));

// For all GET requests, send back index.html
// so that PathLocationStrategy can be used
app.get('/*', function(req, res) {

  console.log(path);
  console.log(__dirname);
  res.sendFile(path.join(__dirname, '/dist/qq-client-app/index.html'));
});

// Start the app by listening on the default
// Heroku port
app.listen(process.env.PORT || 8080);
console.log("node server is running");
