import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {ErrorNotFoundPageComponent} from './error-not-found-page/error-not-found-page.component';
import {MatIconModule} from '@angular/material';
import {ErrorRoutingComponent} from './error-routing.component';
import {ErrorForbiddenPageComponent} from "./error-forbidden-page/error-forbidden-page.component";
import {ErrorForbiddenPageInterceptor} from "qq-core-lib";
import {ErrorNotFoundPageInterceptor} from "qq-core-lib";

@NgModule({
  imports: [
    MatIconModule,
    RouterModule,
    ErrorRoutingComponent,
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: ErrorNotFoundPageInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorForbiddenPageInterceptor, multi: true}
  ],
  declarations: [
    ErrorNotFoundPageComponent,
    ErrorForbiddenPageComponent
  ],
  exports: [
    ErrorNotFoundPageComponent,
    ErrorForbiddenPageComponent
  ]
})
export class ErrorModule {
}

