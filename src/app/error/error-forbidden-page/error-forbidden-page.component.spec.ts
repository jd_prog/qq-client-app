import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ErrorForbiddenPageComponent} from './error-forbidden-page.component';
import {DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';
import {RouterTestingModule} from '@angular/router/testing';
import {BrowserTestingModule} from '@angular/platform-browser/testing';
import {routes} from '../../app-routing.module';


describe('Error Forbidden Page Component', () => {

  let component: ErrorForbiddenPageComponent;
  let componentFixture: ComponentFixture<ErrorForbiddenPageComponent>;
  let de: DebugElement;



  beforeEach( async(() => {
    TestBed.configureTestingModule( {
      declarations: [ErrorForbiddenPageComponent],
      imports: [BrowserTestingModule,
        RouterTestingModule.withRoutes(routes)
      ]
    }).compileComponents();
  }));
  beforeEach( () => {
    componentFixture = TestBed.createComponent(ErrorForbiddenPageComponent);
    component = componentFixture.componentInstance;
    de = componentFixture.debugElement;
    componentFixture.detectChanges();
  });

  it('should create Error Forbidden Page Component', () => {
    expect(component).toBeTruthy();
  });

  it('should contains 403 error int lable html',  () => {
    expect(de.query(By.css('.error403')).nativeElement.innerText).toEqual('403');
  });

  it('should contains `This page not allowed` in html',  () => {
    expect(de.query(By.css('.label-not-page')).nativeElement.innerText).toEqual('This page not allowed');
  });

  it('should be equal to `Go Home` in html',  () => {
    expect(de.query(By.css('.link-to-main-page')).nativeElement.innerText).toEqual('Go Home');
  });

});
