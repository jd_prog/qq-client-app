import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ErrorNotFoundPageComponent} from './error-not-found-page.component';
import {BrowserTestingModule} from '@angular/platform-browser/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';
import {routes} from '../../app-routing.module';
import {MaterialModule} from '../../material/material.module';


describe('Error Not Found Page Component', () =>  {

  let component: ErrorNotFoundPageComponent;
  let componentFixture: ComponentFixture<ErrorNotFoundPageComponent>;
  let de: DebugElement;

  beforeEach( async( () => {
    TestBed.configureTestingModule( {
      declarations: [ErrorNotFoundPageComponent],
      imports: [
        BrowserTestingModule,
        RouterTestingModule.withRoutes(routes),
        MaterialModule
      ]
    }).compileComponents();
  }));
  beforeEach( () => {
    componentFixture = TestBed.createComponent(ErrorNotFoundPageComponent);
    component = componentFixture.componentInstance;
    de = componentFixture.debugElement;
    componentFixture.detectChanges();
  });


  it('should create Error Not Found Page',  () => {
    expect(component).toBeTruthy();
  });

  it('should be equal to `404', () => {
    expect(de.query(By.css('.error404')).nativeElement.innerText).toEqual('404');
  });

  it('should be equal to `Page not found` ',  () => {
    expect(de.query(By.css('.label-not-page')).nativeElement.innerText).toEqual('Page not found');
  });
  it('should be equal to `Go Home`',  () => {
    expect(de.query(By.css('.link-to-main-page')).nativeElement.innerText).toEqual('Go Home');
  });
});
