/**
 * Created by vlad on 20.04.18.
 */
import {Component, Input} from '@angular/core';
import {Router} from '@angular/router';

@Component ({
  selector: 'app-error-not-found-page',
  templateUrl: 'error-not-found-page.component.html',
  styleUrls: ['error-not-found-page.component.less']
})
export class ErrorNotFoundPageComponent {
  constructor(private router: Router) {}
}
