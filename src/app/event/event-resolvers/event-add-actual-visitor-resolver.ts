import {Injectable} from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {EventsService, Oauth2Service, Speaker, Event, EventActualVisitorsService, UserDetailsService, User} from 'qq-core-lib';




@Injectable({
  providedIn: 'root'
})
export class EventAddActualVisitorResolver implements Resolve<User> {
  constructor(private eventService: EventsService,
              private activatedRouter: ActivatedRoute,
              private eventActualVisitorService: EventActualVisitorsService,
              private oauthService: Oauth2Service,
              private router: Router
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User> | Promise<User> | User {
    return this.oauthService
      .requestUserInfo()
      .flatMap(user => this.eventActualVisitorService.addEventActualVisitor(route.params.eventId, user));
  }



}
