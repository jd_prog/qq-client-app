/**
 * Created by yana on 20.04.18.
 */
export * from './events-client/events-client.component';
export * from './events-client-list-item/events-client-list-item.component';
export * from './events-upcoming-client-list/events-upcoming-client-list.component';
export * from './event-client-details/event-client-details.component';
export * from './event-client-about/event-client-about.component';
export * from './events-passed-client-list/events-passed-client-list.component';
export * from './events-client-list/events-client-list.component';
