/**
 * Created by yana on 20.04.18.
 */
import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Event, EventsService, EventStatusService} from 'qq-core-lib';
import {Subscription} from 'rxjs/Subscription';
import {EventHeaderService} from 'qq-core-lib';


@Component({
  selector: 'app-events-client-list',
  styleUrls: ['./events-client-list.component.less'],
  template: ''
})export class EventsClientListComponent implements OnInit, OnDestroy {

  events: Event[];

  private eventsSubscription;

  constructor(private router: Router,
              private eventsService: EventsService,
              private eventHeaderService: EventHeaderService,
              private eventStatusService: EventStatusService) {
  }

  ngOnInit() {
    this.subscribeOnEvents();
    this.eventHeaderService.subject.subscribe( events => {
      this.events = events;
    });
  }

  ngOnDestroy() {
  }

  // this has to be arrow functions to bind this
  isUpcomingEvent = (event: Event) => this.eventStatusService.notPassedEvents(event);

  isPassedEvent = (event: Event) => this.eventStatusService.passed(event);

  private subscribeOnEvents() {
    this.eventsSubscription = this.eventsService.getEventsContainer()
      .subscribe(events => this.events = events);
  }
}
