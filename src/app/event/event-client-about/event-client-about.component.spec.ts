import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {EventClientAboutComponent} from './event-client-about.component';
import {routes} from '../../app-routing.module';
import {RouterTestingModule} from '@angular/router/testing';
import {BrowserTestingModule} from '@angular/platform-browser/testing';
import {MaterialModule} from '../../material/material.module';
import {
  Event,
  EventPotentialVisitorsService,
  EventsService, GoogleGeocoderService, Location, User,
  UserDetailsService,
} from 'qq-core-lib';
import {DebugElement, InjectionToken} from '@angular/core';
import {of} from 'rxjs';
import {CommonModule} from '@angular/common';
import {By} from '@angular/platform-browser';




describe( 'Event Client About Component', () => {
  let component: EventClientAboutComponent;
  let componentFixture: ComponentFixture<EventClientAboutComponent>;
  let de: DebugElement;


  let eventsService;
  let userDetailsService;
  let eventPotenttialVisitorsService;
  let googleGeocoderService: GoogleGeocoderService;

  beforeEach(async(() => {
    eventsService = jasmine.createSpyObj('EventsService', ['findById']);
    userDetailsService = jasmine.createSpyObj('UserDetailsService', ['getCurrentUserUsername']);
    eventPotenttialVisitorsService = jasmine.createSpyObj('EventPotentialVisitorsService', ['getAllPotentialVisitors']);
    googleGeocoderService = jasmine.createSpyObj('GoogleGeocoderService', ['findAddress']);


    TestBed.configureTestingModule( {
      declarations: [EventClientAboutComponent],
      imports: [BrowserTestingModule,
        RouterTestingModule.withRoutes(routes),
        MaterialModule,
        CommonModule],
      providers: [
        {provide: EventsService, useValue: eventsService},
        {provide: UserDetailsService, useValue: userDetailsService},
        {provide: EventPotentialVisitorsService, useValue: eventPotenttialVisitorsService},
        {provide: GoogleGeocoderService, useValue: googleGeocoderService}
      ]
    }).compileComponents().then( () => {
      componentFixture = TestBed.createComponent(EventClientAboutComponent);
      component = componentFixture.componentInstance;
      const username = 'username';

      userDetailsService.getCurrentUserUsername.and.returnValue(of(username));

      const event = new Event();
      event.id = 1;
      event.title = 'Event name';
      event.creator = new User();
      event.dateStart = new Date();
      event.dateEnd = new Date();
      event.description = 'some description';
      event.location = new Location();
      event.location.id = 22;
      event.location.longitude = 24.6964151834693;
      event.location.latitude = 48.91451974590477;
      event.location.placeId = 'ChIJQYW8sz7BMEcRltrnltXXDvY';
      event.location.formattedAddress = 'city name';
      event.participantsLimit = 20;
      const potentialVisitors = new Array<User>();
      eventsService.findById.and.returnValue(of(event));
      eventPotenttialVisitorsService.getAllPotentialVisitors.and.returnValue(of(potentialVisitors));
      component.event = event;
      component.potentialVisitors = potentialVisitors;
      localeContext.window.location.href = '';
    });
  }));


  beforeEach( () => {
    de = componentFixture.debugElement;
    componentFixture.detectChanges();
  });


  it('should have create Event Client About Component',  () => {
    expect(component).toBeTruthy();
  });

  it('should contain Event title name',  () => {
    expect(component.event.title).toBe('Event name');
  });

  it('should contain Event title name in the html',  () => {
    expect(de.query(By.css('.title')).nativeElement.innerText).toEqual('Event name');
  });


  it('should contain Event description in the html',  () => {
    expect(de.query(By.css('.description')).nativeElement.innerText).toEqual('some description');
  });

  it('should contain Event formatted adrress in the html',  () => {
    expect(de.query(By.css('.location')).nativeElement.innerText).toEqual('city name');
  });


  it('should open googleMaps in browser', () => {
    if (component.event.location !== undefined && component.event.location !== null) {
     localeContext.window.location.href = `https://www.google.com/maps/search/?api=1&query=${component.event.location.latitude},${component.event.location.longitude}
      &query_place_id=${component.event.location.placeId}`;
    }
    const currentWindowLocation = `https://www.google.com/maps/search/?api=1&query=${component.event.location.latitude},${component.event.location.longitude}
      &query_place_id=${component.event.location.placeId}`;
    expect(localeContext.window.location.href).toEqual(currentWindowLocation);
  });


  it('should not open  googleMaps in browser', () => {
       component.event.location = null;
    if (component.event.location !== undefined && component.event.location !== null) {
        localeContext.window.location.href = `https://www.google.com/maps/search/?api=1&query=
      ${component.event.location.latitude},${component.event.location.longitude}&query_place_id=${component.event.location.placeId}`;
    }
      const currentWindowLocation = '';
      expect(localeContext.window.location.href).toEqual(currentWindowLocation);
  });

});



 const localeContext = {
   window: {
     location: {
       href: ''
     }
   }
 };
