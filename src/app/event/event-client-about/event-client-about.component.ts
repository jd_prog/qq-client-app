import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {Observable} from 'rxjs/Observable';
import {tap} from 'rxjs/operators';
import {merge} from 'rxjs/internal/observable/merge';
import { empty, of } from 'rxjs';

import {
  Event,
  EventPotentialVisitorsService,
  EventsService,
  EventStatusService,
  GoogleGeocoderService,
  ParticipantsService,
  UserJoinNotificationService,
  UserDetailsService, User
} from 'qq-core-lib';


/**
 * Created by yana on 24.04.18.
 */
@Component({
  selector: 'app-event-client-about',
  templateUrl: './event-client-about.component.html',
  styleUrls: ['./event-client-about.component.less']
})
export class EventClientAboutComponent implements OnInit, OnDestroy {

  public event: Event;
  private eventSubscription: Subscription;
  public joinEventSubscription: Subscription;
  public potentialVisitors = new Array<User>();
  public user: User;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private eventsService: EventsService,
              private evensStatusService: EventStatusService,
              private googleGeocoderService: GoogleGeocoderService,
              private userDetailsService: UserDetailsService,
              private eventPotenttialVisitorsService: EventPotentialVisitorsService,
              private userJoinNotificationService: UserJoinNotificationService) {
  }

  ngOnInit(): void {
    this.userDetailsService.getCurrentUserUsername().subscribe(username => {
      this.user = new User();
      this.user.username = username;
    });

    this.userJoinNotificationService.subject.subscribe( notification => {
      if (notification === 'join') {
        this.potentialVisitors.push(this.user);
      } else {
        this.potentialVisitors = this.potentialVisitors.filter(us => us.username !== this.user.username);
      }
    });
    this.route.params.subscribe( params => {
      this.loadEvent(params['eventId']);
    });
  }


  ngOnDestroy(): void {
    if (this.joinEventSubscription) {
      this.joinEventSubscription.unsubscribe();
    }
  }

  private loadEvent(eventId: number) {
  this.eventsService.findById(eventId).subscribe( event => {
    this.event = event;
    this.eventPotenttialVisitorsService.getAllPotentialVisitors(this.event.id).subscribe( potentialVisitorors => {
        this.potentialVisitors = potentialVisitorors;
        if (!this.event.location.formattedAddress) {
          this.findAddress(this.event.location.latitude, this.event.location.longitude).subscribe( formatedAddress => {
            this.event.location.formattedAddress = formatedAddress;
          });
        }
    });
  });
  }


  private findAddress(latitude: number, longitude: number): Observable<string> {
    return this.googleGeocoderService.findAddress(latitude, longitude)
      .pipe(tap(address => this.event.location.formattedAddress = address));
  }

  openGoogleMaps() {
    if (this.event.location !== null || this.event.location !== undefined) {
      window.location.href = `https://www.google.com/maps/search/?api=1&query=${this.event.location.latitude},${this.event.location.longitude}
      &query_place_id=${this.event.location.placeId}`;
    }
  }

}
