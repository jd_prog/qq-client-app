/**
 * Created by yana on 20.04.18.
 */
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EventsService} from 'qq-core-lib';
import {Subscription} from 'rxjs/Subscription';
import {Event, EventStatusService} from 'qq-core-lib';

@Component({
  selector: 'app-client-events',
  templateUrl: './events-client.component.html',
  styleUrls: ['./events-client.component.less']
})
export class EventsClientComponent implements OnInit {
  events: Event[];
  tabLinks = [
    {
      'path': 'upcoming',
      'label': 'Upcoming'
    },
    {
      'path': 'passed',
      'label': 'Passed'
    }];

  private eventsSubscription: Subscription;

  constructor(public router: Router,
              private activatedRoute: ActivatedRoute,
              private eventsService: EventsService,
              private eventStatusService: EventStatusService) {
  }

  ngOnInit() {
    this.loadEvents();
  }

  // this has to be arrow functions to bind this
  isUpcomingEvent = (event: Event) => this.eventStatusService.notPassedEvents(event);

  private loadEvents() {
    this.eventsSubscription = this.eventsService.findPublished()
      .subscribe(events => this.events = events);
  }

}
