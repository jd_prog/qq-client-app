import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {Component, CUSTOM_ELEMENTS_SCHEMA, DebugElement, Type} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BrowserTestingModule} from '@angular/platform-browser/testing';
import {MaterialModule} from '../../material/material.module';
import {EventsClientComponent} from './events-client.component';
import {EventsService, PipesModule, Event} from 'qq-core-lib';
import {of} from 'rxjs';

import {routes} from '../../app-routing.module';

describe('Events Client Component ', () => {

  let component: EventsClientComponent;
  let componentFixture: ComponentFixture<EventsClientComponent>;
  let de: DebugElement;
  let eventsService;
  const events = [ new Event(), new Event() ];

  beforeEach( async(() => {
    eventsService = jasmine.createSpyObj('EventsService', ['findPublished']);
    TestBed.configureTestingModule( {
      declarations: [EventsClientComponent],
      imports: [BrowserTestingModule,
        RouterTestingModule.withRoutes(routes),
        MaterialModule,
        CommonModule,
        PipesModule],
      providers: [
        {provide: EventsService, useValue: eventsService}
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents().then( () => {
      componentFixture = TestBed.createComponent(EventsClientComponent);
      component = componentFixture.componentInstance;
      eventsService.findPublished.and.returnValues(of(events));
    });
  }));

  beforeEach( () => {
    de = componentFixture.debugElement;
    componentFixture.detectChanges();
  });


  it('should be create Events Client Component',  () => {
      expect(component).toBeTruthy();
  });

  it('should have tab Links',  () => {
    expect(component.tabLinks[0].path).toEqual('upcoming');
    expect(component.tabLinks[0].label).toEqual('Upcoming');
    expect(component.tabLinks[1].path).toEqual('passed');
    expect(component.tabLinks[1].label).toEqual('Passed');
  });

});
