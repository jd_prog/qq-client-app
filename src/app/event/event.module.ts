import {NgModule} from '@angular/core';
import {
  EventClientAboutComponent,
  EventClientDetailsComponent,
  EventsClientComponent,
  EventsUpcomingClientListComponent,
  EventsClientListItemComponent,
  EventsPassedClientListComponent,
  EventsClientListComponent
} from './index';
import {MaterialModule} from '../material/material.module';
import {SpeakerModule} from '../speaker/speaker.module';
import {CommonModule} from '@angular/common';
import {EventRoutingModule} from './event-routing.module';
import {FormsModule} from '@angular/forms';
import {ClientCoreModule} from '../client-core/client-core.module';
import {SecurityModule} from '../security/security.module';
import {NgArrayPipesModule} from 'ngx-pipes';
import {environment} from '../../environments/environment';
import {PipesModule} from 'qq-core-lib';
import { AddActualVisitorComponent } from './add-actual-visitor/add-actual-visitor.component';


@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    SpeakerModule,
    ClientCoreModule,
    EventRoutingModule,
    FormsModule,
    SecurityModule,
    NgArrayPipesModule,
    PipesModule
  ],
  declarations: [
    EventsClientComponent,
    EventClientAboutComponent,
    EventClientDetailsComponent,
    EventsUpcomingClientListComponent,
    EventsClientListItemComponent,
    EventsPassedClientListComponent,
    EventsClientListComponent,
    AddActualVisitorComponent

  ],
  exports: [
    EventsClientComponent,
    EventClientAboutComponent,
    EventClientDetailsComponent,
    EventsUpcomingClientListComponent,
    EventsClientListItemComponent,
    AddActualVisitorComponent
  ]
})
export class EventModule {
}
