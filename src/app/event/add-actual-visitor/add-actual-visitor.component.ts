import { Component, OnInit } from '@angular/core';
import {EventsService, Oauth2Service, UserDetailsService, Event, User} from 'qq-core-lib';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from 'angular5-social-login';

@Component({
  selector: 'app-add-actual-visitor',
  templateUrl: './add-actual-visitor.component.html',
  styleUrls: ['./add-actual-visitor.component.less']
})
export class AddActualVisitorComponent implements OnInit {

  user: User;

  constructor(private eventService: EventsService,
              private activatedRouter: ActivatedRoute,
              private router: Router
              ) { }

  ngOnInit() {
    this.user = this.activatedRouter.snapshot.data['user'];
    this.activatedRouter.params.subscribe( params => {
      this.router.navigate( ['events', params['eventId'] , 'streams'], {replaceUrl: true});
    });
  }

}
