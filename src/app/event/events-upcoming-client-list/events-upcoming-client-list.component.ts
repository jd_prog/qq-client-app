/**
 * Created by yana on 20.04.18.
 */
import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {EventsService, EventStatusService} from 'qq-core-lib';
import {EventsClientListComponent} from '../events-client-list/events-client-list.component';
import {EventHeaderService} from 'qq-core-lib';

@Component({
  selector: 'app-events-upcoming-client-list',
  templateUrl: './events-upcoming-client-list.component.html',
  styleUrls: ['../events-client-list/events-client-list.component.less']
})
export class EventsUpcomingClientListComponent extends EventsClientListComponent {

  constructor( router: Router,
               eventsService: EventsService,
               eventHeaderService: EventHeaderService,
               eventStatusService: EventStatusService) {
    super(router, eventsService,  eventHeaderService, eventStatusService);
  }


}
