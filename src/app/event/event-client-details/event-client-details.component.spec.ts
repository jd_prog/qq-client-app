import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {CommonModule} from '@angular/common';
import {BrowserTestingModule} from '@angular/platform-browser/testing';
import {MaterialModule} from '../../material/material.module';

import {EventClientDetailsComponent} from './event-client-details.component';
import {CUSTOM_ELEMENTS_SCHEMA, DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';
import {routes} from '../../app-routing.module';
import {Router} from '@angular/router';



describe('Event Client Details Component', () => {
  let component: EventClientDetailsComponent;
  let componentFixture: ComponentFixture<EventClientDetailsComponent>;
  let de: DebugElement;


  beforeEach( async(() => {

    TestBed.configureTestingModule( {
      declarations: [
        EventClientDetailsComponent],
      imports: [BrowserTestingModule,
        RouterTestingModule.withRoutes(routes),
        MaterialModule,
        CommonModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));
  beforeEach( () => {
    componentFixture = TestBed.createComponent(EventClientDetailsComponent);
    component = componentFixture.componentInstance;
    de = componentFixture.debugElement;
    componentFixture.detectChanges();
  });

  it('should create Event Client Details Component ',  () => {
    expect(component).toBeTruthy();
  });

  it('should be equal `link label`',  () => {
    expect(de.query(By.css('.nav-bar-tab')).nativeElement.innerText).toEqual('LINK.LABEL');
  });

  it('should be equal `Topics and Streams`',  () => {
    expect(de.query(By.css('.join-event-btn')).nativeElement.innerText).toEqual('Topics and Streams');
  });


  it('expect component links and labels to be equal',  () => {
    expect(component.tabLinks[0].path).toEqual('about');
    expect(component.tabLinks[0].label).toEqual('Details');
    expect(component.tabLinks[1].path).toEqual('speakers');
    expect(component.tabLinks[1].label).toEqual('Speakers');
  });

});
