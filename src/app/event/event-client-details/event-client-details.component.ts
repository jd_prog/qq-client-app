import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {
  Event,
  EventsService,
  EventStatusService,
  GoogleGeocoderService,
  Oauth2Service,
  ParticipantsService,
  UserDetailsService,
  Visitor
} from 'qq-core-lib';
import {Observable} from 'rxjs/Observable';
import {map, tap} from 'rxjs/operators';
import {combineLatest, Subscription} from 'rxjs';
import {merge} from 'rxjs/internal/observable/merge';

/**
 * Created by yana on 24.04.18.
 */
@Component({
  selector: 'app-client-event-details',
  templateUrl: './event-client-details.component.html',
  styleUrls: ['./event-client-details.component.less']
})
export class EventClientDetailsComponent {

  public event: Event;
  public joinedToEvent;
  private eventSubscription: Subscription;
  private joinEventSubscription: Subscription;

  tabLinks = [
    {
      'path': 'about',
      'label': 'Details'
    },
    {
      'path': 'speakers',
      'label': 'Speakers'
    }];


  constructor(private router: Router,
              private activatedRoute: ActivatedRoute) {}
  openStreams() {
    this.router.navigate([`streams`], {relativeTo: this.activatedRoute});
  }
}
