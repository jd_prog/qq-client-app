/**
 * Created by yana on 20.04.18.
 */
import {Component, Input} from '@angular/core';
import {Event} from 'qq-core-lib';
import {Router} from '@angular/router';

@Component({
  selector: 'app-client-events-list-item',
  templateUrl: './events-client-list-item.component.html',
  styleUrls: ['./events-client-list-item.component.less']
})
export class EventsClientListItemComponent {

  @Input('event')
  event: Event = new Event();

  constructor(private router: Router) {
  }

  openEventDetails(event: Event) {
    this.router.navigate(['events', event.id, 'about']);
  }
}
