/**
 * Created by yana on 20.04.18.
 */
import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RouterTestingModule} from '@angular/router/testing';
import {EventsClientListItemComponent} from './events-client-list-item.component';

describe('EventsListItemComponent', () => {
  let component: EventsClientListItemComponent;
  let fixture: ComponentFixture<EventsClientListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        EventsClientListItemComponent
      ],
      imports: [
        RouterTestingModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventsClientListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
