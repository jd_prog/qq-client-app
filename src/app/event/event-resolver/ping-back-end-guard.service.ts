import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {PingBackEndService} from 'qq-core-lib';


@Injectable({
  providedIn: 'root'
})
export class PingBackEndGuard implements CanActivate {

  constructor(private pinkBackEndService: PingBackEndService,
              private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.pinkBackEndService.startServer().map( request => {
      if(request) {return true;
      }}
    ).catch( () => {
       this.router.navigate(['login'], {replaceUrl: true});
      return Observable.of(false);
    });
  }
}
