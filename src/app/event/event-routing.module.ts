import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ClientAuthGuard, ClientEventPermeationGuard} from 'qq-core-lib';
import {EventsClientComponent} from './events-client/events-client.component';
import {EventsUpcomingClientListComponent} from './events-upcoming-client-list/events-upcoming-client-list.component';
import {EventsPassedClientListComponent} from './events-passed-client-list/events-passed-client-list.component';
import {EventClientDetailsComponent} from './event-client-details/event-client-details.component';
import {SpeakerListComponent} from '../speaker';
import {EventClientAboutComponent} from './event-client-about/event-client-about.component';
import {AddActualVisitorComponent} from './add-actual-visitor/add-actual-visitor.component';
import {EventAddActualVisitorResolver} from './event-resolvers/event-add-actual-visitor-resolver';
import {PingBackEndGuard} from './event-resolver/ping-back-end-guard.service';

export const routes: Routes = [
  {path: 'events', redirectTo: 'events/upcoming'},
  {
    path: 'events',
    component: EventsClientComponent,
    canActivate: [ PingBackEndGuard],
    children: [
      {path: 'upcoming', component: EventsUpcomingClientListComponent},
      {path: 'passed', component: EventsPassedClientListComponent}
    ],
  },
  {path: 'events/:eventId', redirectTo: 'events/:eventId/about'},
  {
    path: 'events/:eventId',
    component: EventClientDetailsComponent,
    canActivate: [ClientEventPermeationGuard , PingBackEndGuard],
    children: [
      {path: 'about', component: EventClientAboutComponent},
      {path: 'speakers', component: SpeakerListComponent}
    ]
  },
  {path: 'events/:eventId/add/actualvisitor', component: AddActualVisitorComponent, resolve: {user: EventAddActualVisitorResolver} , canActivate: [ClientAuthGuard , PingBackEndGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class EventRoutingModule {
}
