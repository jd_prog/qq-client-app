
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {StreamClientListComponent} from './stream-client-list.component';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '../../material/material.module';
import {CUSTOM_ELEMENTS_SCHEMA, DebugElement} from '@angular/core';
import {RouterTestingModule} from '@angular/router/testing';
import {BrowserTestingModule} from '@angular/platform-browser/testing';
import {routes} from '../../app-routing.module';
import {EventsService, StreamService, Event, Stream} from 'qq-core-lib';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {of} from 'rxjs';

describe('Stream Client List Component', () => {

  let component: StreamClientListComponent;
  let componentFixture: ComponentFixture<StreamClientListComponent>;
  let de: DebugElement;

  let streamService;
  let eventsService;

  const event = new Event();
  const streams = [];

  beforeEach( async(() => {


    streamService = jasmine.createSpyObj('StreamService', ['getStreamsByEvent']);
    eventsService = jasmine.createSpyObj('EventsService', ['findById']);

    TestBed.configureTestingModule({
      declarations: [StreamClientListComponent],
      imports: [
        BrowserTestingModule,
        RouterTestingModule.withRoutes(routes),
        HttpClientTestingModule,
        CommonModule,
        MaterialModule,
      ],
      providers: [
        {provide: StreamService, useValue: streamService},
        {provide: EventsService, useValue: eventsService}],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents().then(() => {
      componentFixture = TestBed.createComponent(StreamClientListComponent);
      component = componentFixture.componentInstance;
      streamService.getStreamsByEvent.and.returnValue( of(streams));
      eventsService.findById.and.returnValue( of(event));
    });
  }));


  beforeEach(() => {
    de = componentFixture.debugElement;
    componentFixture.detectChanges();
  });


  it('should have been created',  () => {
      expect(component).toBeTruthy();
  });

  it('should have event',  () => {
    componentFixture.detectChanges();
    expect(component.event).toEqual(event);
  });


  it('should have stream list', () => {
    componentFixture.detectChanges();
    expect(component.streams).toEqual(streams);
  });


  it('should return event by id', () => {
     eventsService.findById().subscribe(next => {
       console.log(component.event);
       expect(next).toEqual(event);
     });
  });

  it('should return stream list by event id', () => {
    streamService.getStreamsByEvent().subscribe( next => {
      expect(next).toEqual(streams);
    });
  });
});
