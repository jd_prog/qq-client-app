import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Event, Stream, StreamService, EventsService} from 'qq-core-lib';

@Component({
  selector: 'app-stream-client-list',
  templateUrl: './stream-client-list.component.html',
  styleUrls: ['./stream-client-list.component.less'],
  providers: [StreamService]
})
export class StreamClientListComponent implements OnInit {


  streams = [];
  event = new  Event();
  eventId?: string;
  constructor(private activatedRoute: ActivatedRoute ,
              private streamService: StreamService ,
              private eventService: EventsService,
              private route: Router
              ) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe( urlParams => {
      this.eventId = urlParams['eventId'];
    } );
    this.streamService.getStreamsByEvent(this.eventId).subscribe( streams => {
      this.streams = streams;
      if (this.streams.length !== 0) {
        this.route.navigate(['events', this.eventId, 'streams' , this.streams[0].id], {replaceUrl: true});
      }
    });
    this.eventService.findById(+this.eventId).subscribe( event => {
      this.event = event;
    });
  }
}


