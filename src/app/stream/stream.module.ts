import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {StreamClientListComponent} from './index';
import {MaterialModule} from '../material/material.module';
import {StreamRoutingModule} from './stream-routing.module';
import {ClientCoreModule} from '../client-core/client-core.module';
import {TopicModule} from '../topic/topic.module';
import {FormsModule} from '@angular/forms';
import {SecurityModule} from '../security/security.module';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    ClientCoreModule,
    StreamRoutingModule,
    TopicModule,
    FormsModule,
    SecurityModule
  ],
  declarations: [
    StreamClientListComponent
  ],
  exports: [
    StreamClientListComponent
  ]
})
export class StreamModule { }
