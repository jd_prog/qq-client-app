import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {TopicClientListComponent} from '../topic';
import {StreamClientListComponent} from './stream-client-list/stream-client-list.component';
import {ClientAuthGuard} from 'qq-core-lib';
import {PingBackEndGuard} from '../event/event-resolver/ping-back-end-guard.service';

const routes: Routes = [
  {path: 'events/:eventId/streams', component: StreamClientListComponent , canActivate: [ PingBackEndGuard],
    children: [
      {path: ':id', component: TopicClientListComponent }
    ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class StreamRoutingModule {
}
