import {NgModule} from '@angular/core';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatTabsModule} from '@angular/material';
import {MatButtonModule} from '@angular/material/button';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {MatButtonToggleModule} from '@angular/material/button-toggle';


@NgModule({
  imports: [
    CommonModule,
    MatTabsModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule ,
    MatFormFieldModule,
    MatInputModule ,
    ReactiveFormsModule,
    MatButtonToggleModule

  ],
  exports: [
    MatTabsModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule ,
    MatFormFieldModule,
    MatInputModule ,
    ReactiveFormsModule,
    MatButtonToggleModule
  ]
})
export class MaterialModule {

  constructor() {}
}
