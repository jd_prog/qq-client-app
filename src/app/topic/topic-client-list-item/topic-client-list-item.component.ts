import {Component, Input} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TopicStatusService, Topic} from 'qq-core-lib';

@Component({
  selector: 'app-topic-client-list-item',
  templateUrl: './topic-client-list-item.component.html',
  styleUrls: ['./topic-client-list-item.component.less']
})
export class TopicClientListItemComponent {

  @Input('topic')
  topic: Topic;

  constructor( private navigator: Router,
               private activatedRoute: ActivatedRoute,
               private topicStatusService: TopicStatusService) { }


  isActive(): boolean {
    return this.topicStatusService.ongoing(this.topic);
  }


  goToTopicsDetails(topic: Topic) {
    this.navigator.navigate(['topic', topic.id], {relativeTo: this.activatedRoute});
  }
}
