import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TopicClientListItemComponent} from './topic-client-list-item.component';
import {BrowserTestingModule} from '@angular/platform-browser/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {routes} from '../../app-routing.module';
import {TopicStatusService} from 'qq-core-lib';

describe('TopicClientListItemComponent', () => {
  let component: TopicClientListItemComponent;
  let fixture: ComponentFixture<TopicClientListItemComponent>;
  let topicStatusService;
  beforeEach(async(() => {
    topicStatusService = jasmine.createSpyObj('TopicStatusService', ['ongoing']);
    TestBed.configureTestingModule({
      declarations: [
        TopicClientListItemComponent
      ],
      imports: [
        BrowserTestingModule,
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [
        {provide: TopicStatusService, useValue: topicStatusService}
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopicClientListItemComponent);
    component = fixture.componentInstance;
    const topic = {id: 1, description: 'describe', title: 'tisp', users: [], startDate: new Date(), endDate: new Date() };
    component.topic = topic;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
