import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {CommonModule} from '@angular/common';
import {PipesModule, Stream, StreamService, Topic, TopicService, UserDetailsService} from 'qq-core-lib';
import {MaterialModule} from '../../material/material.module';
import {BrowserTestingModule} from '@angular/platform-browser/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {routes} from '../../app-routing.module';
import {TopicClientDetailsComponent} from './topic-client-details.component';
import {CUSTOM_ELEMENTS_SCHEMA, DebugElement} from '@angular/core';
import {Observable, of} from 'rxjs';


describe('Topic Client Details Component', () => {
  let component: TopicClientDetailsComponent;
  let componentFixture: ComponentFixture<TopicClientDetailsComponent>;
  let de: DebugElement;

  let topicService;
  let streamsService;
  let userDetailsService;
  beforeEach( async(() => {
    topicService = jasmine.createSpyObj('TopicService', ['findById'] );
    streamsService = jasmine.createSpyObj('StreamService', ['getStreamById']);
    userDetailsService = jasmine.createSpyObj('UserDetailsService', ['getCurrentUserUsername']);
    TestBed.configureTestingModule( {
      declarations: [TopicClientDetailsComponent],
      imports: [HttpClientTestingModule,
        BrowserTestingModule,
        RouterTestingModule.withRoutes(routes),
        CommonModule,
        PipesModule,
        MaterialModule],
      providers: [
        {provide: TopicService, useValue: topicService},
        {provide: StreamService, useValue: streamsService},
        {provide: UserDetailsService, useValue: userDetailsService}
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));
  beforeEach( () => {
    componentFixture = TestBed.createComponent(TopicClientDetailsComponent);
    component = componentFixture.componentInstance;
    de = componentFixture.debugElement;
  });
  it('should be created Topic Client Details Component',  () => {
    expect(component).toBeTruthy();
  });

  it('should Topic Client Details Component #loadTopic', async(() => {
    const topic = new Topic();
    const expectedTopic = of(topic);
    topicService.findById.and.returnValue( expectedTopic );
    component.loadTopic('1').subscribe( topic1 => {
      expect(topic1).toEqual(topic);
    });
  }));


  it('should Topic Client Details Component #loadStream', async(() => {
    const stream = new Stream(1, 'location', 'name', []);
    const expectedStream = of(stream);
    streamsService.getStreamById.and.returnValue(of(stream));
    component.loadStream('4').subscribe( observableStream => {
      expect(observableStream).toEqual(stream);
    });
  }));


});
