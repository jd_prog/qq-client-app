import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Topic, Stream, StreamService, TopicService, UserDetailsService, Oauth2Service} from 'qq-core-lib';
import {of, Subscription} from 'rxjs';
import * as moment from 'moment';
import {tap} from 'rxjs/operators';
import {Observable} from 'rxjs/Observable';
import {merge} from 'rxjs/internal/observable/merge';
import {AuthService} from 'angular5-social-login';



/**
 * Created by yana on 24.04.18.
 */
@Component({
  selector: 'app-topic-client-details',
  templateUrl: './topic-client-details.component.html',
  styleUrls: ['./topic-client-details.component.less']
})
export class TopicClientDetailsComponent implements OnInit, OnDestroy {

  public topic: Topic;
  private topicSubscription: Subscription;

  public isAuthorized = false;
  streamName: String;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private topicService: TopicService,
              private streamsService: StreamService,
              private userDetailsService: UserDetailsService) {
  }

  ngOnInit(): void {
    this.topicSubscription = this.route.params
      .flatMap(params => merge(this.loadTopic(params['topicId']), this.loadStream(params['id']), this.isUserAuthorized()))
      .subscribe();
  }

  ngOnDestroy(): void {
    this.topicSubscription.unsubscribe();
  }

  public loadTopic(topicId: string): Observable<Topic> {
    return this.topicService.findById(+topicId)
      .pipe(tap(topic => this.topic = topic));
  }

  public loadStream(streamId: string): Observable<Stream> {
    return this.streamsService.getStreamById(streamId)
      .pipe(tap(stream => this.streamName = stream.name));
  }

  public showQuestions() {
    this.router.navigate(['questions', 'upcoming'], {relativeTo: this.route});
  }


  calculateDuration(): number {
    return moment.duration(moment(this.topic.endDate).diff(moment(this.topic.startDate))).asMinutes();
  }

  isUserAuthorized() {
    return this.userDetailsService.getCurrentUserUsername()
      .pipe(tap ( username => {
        console.log(username);
        this.isAuthorized = username !== null;
      }));
  }
}
