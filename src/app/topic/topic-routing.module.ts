import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TopicClientDetailsComponent} from './topic-client-details/topic-client-details.component';
import {ClientAuthGuard} from 'qq-core-lib';
import {PingBackEndGuard} from '../event/event-resolver/ping-back-end-guard.service';

const routes: Routes = [
  {path: 'events/:eventId/streams/:id/topic/:topicId', component: TopicClientDetailsComponent, canActivate: [ PingBackEndGuard]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TopicRoutingModule {
}
