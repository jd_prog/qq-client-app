/**
 * Created by yana on 25.04.18.
 */
export * from './topic-client-list/topic-client-list.component';
export * from './topic-client-details/topic-client-details.component';
export * from './topic-client-list-item/topic-client-list-item.component';
