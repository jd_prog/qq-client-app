import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {TopicClientListComponent} from './topic-client-list.component';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '../../material/material.module';
import {BrowserTestingModule} from '@angular/platform-browser/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {routes} from '../../app-routing.module';
import {CUSTOM_ELEMENTS_SCHEMA, DebugElement} from '@angular/core';
import {EventsService, EventStatusService, PipesModule, StreamService, TopicService, TopicStatusService, Event, Topic} from 'qq-core-lib';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {of} from 'rxjs';

describe(' Topic Client List Component', () => {

    let component: TopicClientListComponent;
    let componentFixture: ComponentFixture<TopicClientListComponent>;
    let de: DebugElement;

    let topicService ;
    let eventsService ;
    let streamService ;

    const topics = [];
    const streams = [];
    const event = {};


  beforeEach( async(() => {
    topicService = jasmine.createSpyObj('TopicService', ['getTopics']);
    eventsService = jasmine.createSpyObj('EventsService', ['findById']);
    streamService = jasmine.createSpyObj('StreamService', ['getStreamById']);
    TestBed.configureTestingModule( {
      declarations: [TopicClientListComponent],
      imports: [
        HttpClientTestingModule,
        BrowserTestingModule,
        RouterTestingModule.withRoutes(routes),
        CommonModule,
        PipesModule,
        MaterialModule] ,
      providers: [
        {provide: TopicService, useValue: topicService},
        {provide: EventsService, useValue: eventsService},
        {provide: StreamService, useValue: streamService},
        EventStatusService,
        TopicStatusService
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents().then( () => {
      componentFixture = TestBed.createComponent(TopicClientListComponent);
      component = componentFixture.componentInstance;
      topicService.getTopics.and.returnValue( of(topics));
      streamService.getStreamById.and.returnValue( of(streams));
      eventsService.findById.and.returnValue( of(event));
    });
  }));

  beforeEach( () => {
    de = componentFixture.debugElement;
    componentFixture.detectChanges();
  });



  it('should have been create Topic Client list component',  () => {
        expect(component).toBeTruthy();
  });

  it('should return mock of event', async(() =>  {
    const eventsServiceMock = TestBed.get(EventsService);
    eventsServiceMock.findById().subscribe( next => {
      expect(next).toEqual(event);
    });
  }));

  it('should return mock of streams', async(() =>  {
    const streamServiceMock = TestBed.get(StreamService);
    streamServiceMock.getStreamById().subscribe( observableStreams => {
      expect(observableStreams).toEqual(streams);
    });
  }));


  it('should return mock of topics', async(() =>  {
    const topicServiceMock = TestBed.get(TopicService);
    topicServiceMock.getTopics().subscribe( observableTopics => {
      expect(observableTopics).toEqual(topics);
    });
  }));

  it('should check Topic Client list component #isEventNotPassed (false)',  fakeAsync(() =>  {
    const passedEvent  = new Event();
    passedEvent.dateStart = new Date();
    passedEvent.dateEnd = new Date();
    tick(500);
    expect(component.isEventNotPassed(passedEvent)).toBe(false);
  }));


  it('should check Topic Client list component #isEventNotPassed (true)',  fakeAsync(() =>  {
    const notPassedEvent  = new Event();
    notPassedEvent.dateStart = new Date(new Date().getTime() + 4000);
    notPassedEvent.dateEnd = new Date(new Date().getTime() + 4000);
    tick(500);
    expect(component.isEventNotPassed(notPassedEvent)).toBe(true);
  }));

  it('should check Topic Client list component #isOngoing case(date start now , date end after 10000 milis)', () => {
    const topicIsNotPassed = new Topic();
    topicIsNotPassed.startDate = new Date(new Date().getTime());
    topicIsNotPassed.endDate = new Date(new Date(new Date().getTime() + 10000).getTime());
    expect(component.isOngoing(topicIsNotPassed)).toBe(true);
  });


  it('should check Topic Client list component #isOngoing case(date start 10000 milis ago, date end 5000 milis ago)', () => {
    const topicIsNotPassed = new Topic();
    topicIsNotPassed.startDate = new Date(new Date().getTime() - 10000);
    topicIsNotPassed.endDate = new Date(new Date().getTime() - 5000);
    expect(component.isOngoing(topicIsNotPassed)).toBe(false);
  });

  it('should check Topic Client list component #isOngoing case(date start 10000 milis ago, date end 5000 milis from now)', () => {
    const topicIsNotPassed = new Topic();
    topicIsNotPassed.startDate = new Date(new Date().getTime() - 10000);
    topicIsNotPassed.endDate = new Date(new Date().getTime() + 5000);
    expect(component.isOngoing(topicIsNotPassed)).toBe(true);
  });
  it('should check Topic Client list component #isUpcoming case (date start  in 10.000 milis from now , date end after 20000 milis from now)', () => {
    const topicIsNotPassed = new Topic();
    topicIsNotPassed.startDate = new Date(new Date().getTime() + 10000);
    topicIsNotPassed.endDate = new Date(new Date().getTime() + 20000);
    expect(component.isUpcoming(topicIsNotPassed)).toBe(true);
  });
  it('should check Topic Client list component #isUpcoming case (date start 10.000 milis before now , date end after 20000 milis from now)', () => {
    const topicIsNotPassed = new Topic();
    topicIsNotPassed.startDate = new Date(new Date().getTime() - 10000);
    topicIsNotPassed.endDate = new Date(new Date().getTime() + 20000);
    expect(component.isUpcoming(topicIsNotPassed)).toBe(false);
  });




});
