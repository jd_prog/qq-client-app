import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {
  Event,
  Topic,
  Stream,
  EventsService,
  TopicService,
  EventStatusService,
  StreamService,
  TopicStatusService
} from 'qq-core-lib';
import * as moment from 'moment';

@Component({
  selector: 'app-topic-client-list',
  templateUrl: './topic-client-list.component.html',
  styleUrls: ['./topic-client-list.component.less'],
  providers: [TopicService]
})
export class TopicClientListComponent implements OnInit {

  stream: Stream;
  streamId: string;
  event: Event;
  topics = new Array<Topic>();

  constructor(private topicService: TopicService,
              private eventService: EventsService,
              private streamService: StreamService,
              private topicStatusService: TopicStatusService,
              private eventStatusService: EventStatusService,
              public activatedRouter: ActivatedRoute,
              private router: Router) {
  }


  ngOnInit() {
    this.activatedRouter.params.subscribe(params => {
      this.eventService.findById(+params['eventId']).subscribe(event => {
        this.event = event;
          this.streamService.getStreamById(params['id']).subscribe(stream => {
            this.stream = <Stream>stream;
            this.topicService.getTopics(this.stream.id).subscribe(topics => {
              this.topics = <Topic[]>topics;
              this.topics.sort( (it, it2) => moment(it.startDate).unix() - moment(it2.startDate).unix());
            });
          });
      });
    });
  }


  isOngoing = (topic: Topic) => this.topicStatusService.ongoing(topic);

  isUpcoming = (topic: Topic) => this.topicStatusService.upcoming(topic);

  isEventNotPassed(event: Event): boolean {
    return !this.eventStatusService.passed(event);

  }


}
