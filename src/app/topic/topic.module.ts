import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '../material/material.module';
import {TopicClientDetailsComponent, TopicClientListComponent, TopicClientListItemComponent} from './index';
import {SpeakerModule} from '../speaker/speaker.module';
import {ClientCoreModule} from '../client-core/client-core.module';
import {TopicRoutingModule} from './topic-routing.module';
import {SecurityModule} from '../security/security.module';
import {FormsModule} from '@angular/forms';
import {NgPipesModule} from 'ng-pipes';

import {NgArrayPipesModule} from 'ngx-pipes';
import {PipesModule} from 'qq-core-lib';


@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    SpeakerModule,
    ClientCoreModule,
    TopicRoutingModule,
    SecurityModule,
    FormsModule,
    NgPipesModule,
    NgArrayPipesModule,
    PipesModule

  ],
  declarations: [
    TopicClientDetailsComponent,
    TopicClientListItemComponent,
    TopicClientListComponent
  ],
  exports: [
    TopicClientDetailsComponent,
    TopicClientListItemComponent,
    TopicClientListComponent
  ]
})
export class TopicModule {
}
