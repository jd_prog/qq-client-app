import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  SpeakerListComponent,
  SpeakerListItemComponent
} from './index';
import {MaterialModule} from '../material/material.module';
import {ClientCoreModule} from '../client-core/client-core.module';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    ClientCoreModule
  ],
  declarations: [
    SpeakerListComponent,
    SpeakerListItemComponent
  ],
  exports: [
    SpeakerListComponent,
    SpeakerListItemComponent
  ]
})
export class SpeakerModule {
}
