import {Component, OnInit, OnDestroy} from '@angular/core';
import {Speaker, SpeakersService} from 'qq-core-lib';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';

/**
 * Created by yana on 20.04.18.
 */
@Component({
  selector: 'app-speaker-list',
  templateUrl: './speaker-list.component.html',
  styleUrls: ['./speaker-list.component.less']
})
export class SpeakerListComponent implements OnInit, OnDestroy {

  public speakers: Array<Speaker> = [];
  private speakersSubscription: Subscription;
  private urlSubscription: Subscription;

  constructor(private route: ActivatedRoute,
              private speakersService: SpeakersService) {
  }

  ngOnInit(): void {
    this.urlSubscription = this.route.params.subscribe(params => {
      this.uploadSpeakers(params['eventId']);
    });
  }

  ngOnDestroy(): void {
    this.urlSubscription.unsubscribe();
    this.speakersSubscription.unsubscribe();
  }

  private uploadSpeakers(eventId: number) {
    this.speakersSubscription = this.speakersService.findAllByEvent(eventId)
      .subscribe(speakers => {
          this.speakers = speakers.filter((user, index) => {
            return speakers.findIndex(us => us.username === user.username) === index;
          });
        }
      );
  }
}

