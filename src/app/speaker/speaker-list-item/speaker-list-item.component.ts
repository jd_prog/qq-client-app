import {Component, Input} from '@angular/core';
import {Speaker} from 'qq-core-lib';

/**
 * Created by yana on 20.04.18.
 */
@Component({
  selector: 'app-speaker-list-item',
  templateUrl: './speaker-list-item.component.html',
  styleUrls: ['./speaker-list-item.component.less']
})
export class SpeakerListItemComponent {

  @Input('speaker')
  speaker: Speaker;

  constructor() {}
image(photo){
  return 'url('+photo+')';
}
}
