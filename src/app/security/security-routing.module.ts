import {NgModule} from '@angular/core';
import {LoginClientComponent} from './login-client/login-client.component';
import {RouterModule, Routes} from '@angular/router';
import {LoginPageGuard} from 'qq-core-lib';

const routes: Routes = [
  {path: 'login', component: LoginClientComponent, canActivate: [LoginPageGuard], data: {
     destinationRoute: ['events']
  }}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SecurityRoutingModule {
}
