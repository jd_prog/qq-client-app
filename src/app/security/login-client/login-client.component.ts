/**
 * Created by yana on 30.04.18.
 */
import {Component, OnInit} from '@angular/core';
import {AuthService, FacebookLoginProvider, GoogleLoginProvider} from 'angular5-social-login';
import {Oauth2Service} from 'qq-core-lib';
import {ActivatedRoute, Router} from '@angular/router';
import {
  LinkedInService
} from 'angular-linkedin-sdk';
import {LinkedinLoginProvider} from 'angular5-social-auth';
import {environment} from '../../../environments/environment';
import * as process from "process";


@Component({
  selector: 'app-login-client',
  templateUrl: './login-client.component.html',
  styleUrls: ['./login-client.component.less']
})
export class LoginClientComponent implements OnInit {
  returnUrl: string;

  constructor(private socialAuthService: AuthService,
              private oauthService: Oauth2Service,
              private linkedInService: LinkedInService,
              private router: Router,
              private activatedRouter: ActivatedRoute) {
  }


  ngOnInit(): void {
    this.returnUrl = this.activatedRouter.snapshot.queryParams['returnUrl'];
  }

  public loginWithFacebook() {
    if ( environment.production) {
      this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID)
        .then(socialUser => {
            this.login(socialUser.id, socialUser.name, socialUser.image);
          }
        );
    } else {
      console.log(environment.serverUrl);
      this.login('111111111111', 'user', '');
    }
  }

  private  login(username: string, password: string, image: string) {

    this.oauthService.authorize(username, password, image)
      .subscribe(success => {
        if (success) {
          this.router.navigateByUrl(this.returnUrl);
          // this.router.navigate(['events']);
        }
      });

  }
  public loginWithGoogle() {
    if ( environment.production) {
      this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID)
        .then(socialUser => {
          this.login(socialUser.id, socialUser.name, socialUser.image);
        });
    } else {
      console.log(environment.serverUrl);
      this.login('111111111111', 'user', '');
    }
  }

  public loginWithLinkedIn() {
    if ( environment.production) {
      this.socialAuthService.signIn(LinkedinLoginProvider.PROVIDER_ID)
        .then(socialUser => {
          this.login(socialUser.id, socialUser.name, socialUser.image);
        });
    } else {
      console.log(environment.serverUrl);
      this.login('111111111111', 'user', '');
    }
  }
}
