
import {throwError as observableThrowError, Observable} from 'rxjs';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Oauth2Service, UserDetailsService} from 'qq-core-lib';
import {Router, RouterStateSnapshot} from '@angular/router';
import 'rxjs-compat/add/operator/concatMap';
import 'rxjs-compat/add/operator/catch';
import 'rxjs/add/observable/of';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: Oauth2Service,
              private userDetailsService: UserDetailsService,
              private router: Router) {
  }

  intercept(originRequest: HttpRequest<any>, next: HttpHandler) {

    return this.addAuthHeader(originRequest)
      .concatMap(request => next.handle(request))
      .catch(error => {

        switch (error.status) {

          case 401:
            return this.handle401Error(originRequest, next);

          default:
            return observableThrowError(error);
        }
      });
  }

  handle401Error(originRequest: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> | Observable<any> {

    return this.authService.refreshToken()
      .concatMap((success) => {
        if (success) {
          return this.addAuthHeader(originRequest)
            .concatMap(request => next.handle(request));
        }
      })
      .catch(() => this.logout());
  }


  private addAuthHeader(request: HttpRequest<any>): Observable<HttpRequest<any>> {

    const url = request.url;
    if (url.includes('users/register')
      || url.includes('oauth/token') || (request.method === 'GET' && !url.includes('/users/me')) || (request.method === 'POST' && url.includes('/api/events/filter/tags')) || (request.method === 'POST' && url.includes('/api/events/filter/tag'))) {

      return Observable.create(observer => {
        const headers = request.headers.set('Authorization', this.authService.basicAuthToken);

        observer.next(request.clone({headers: headers}));
        observer.complete();

      });

    } else if (!url.includes('maps.googleapis.com') && (url.includes('/api/') || url.includes('/users/me'))) {

      return this.userDetailsService.getCurrentUserAccessToken()
        .map((accessToken: string) => {
          const headers = request.headers.set('Authorization', `bearer ${accessToken}`);
          return request.clone({headers: headers});
        });
    } else {

      return Observable.of(request);
    }
  }

  private logout(): Observable<boolean> {
    return this.authService.logout()
      .concatMap(() => {
        this.router.navigate(['./login'] );
        return this.authService.isAuthenticated();
      });
  }
}
