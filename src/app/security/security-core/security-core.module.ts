import {NgModule} from '@angular/core';
import {
  AuthServiceConfig,
  FacebookLoginProvider,
  GoogleLoginProvider,
  SocialLoginModule
} from 'angular5-social-login';

import {HTTP_INTERCEPTORS} from '@angular/common/http';

import {ClientCoreModule} from '../../client-core/client-core.module';
import {ClientAuthGuard} from 'qq-core-lib';
import {LocalStorageModule} from '@ngx-pwa/local-storage';
import {environment} from '../../../environments/environment';
import { LinkedInSdkModule } from 'angular-linkedin-sdk';
import {LinkedinLoginProvider} from 'angular5-social-auth';
import {AuthInterceptor} from './interceptors/auth.interceptor';


export function getAuthServiceConfigs() {
  return new AuthServiceConfig(
    [{
      id: FacebookLoginProvider.PROVIDER_ID,
      provider: new FacebookLoginProvider('571362676400579')
      },
      {id: GoogleLoginProvider.PROVIDER_ID,
       provider: new GoogleLoginProvider(environment.googleCredentials.web.client_id)
      },
      { id: LinkedinLoginProvider.PROVIDER_ID,
        provider: new LinkedinLoginProvider(environment.linkedId.client_id)
       }
       ]);
}

@NgModule({
  imports: [
    SocialLoginModule,
    LocalStorageModule,
    ClientCoreModule,
    LinkedInSdkModule
  ],
  providers: [
    { provide: 'apiKey', useValue: environment.linkedId.client_id },
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    },
    ClientAuthGuard
  ]
})
export class SecurityCoreModule {
}
