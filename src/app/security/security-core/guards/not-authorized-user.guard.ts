import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {UserDetailsService} from 'qq-core-lib';
import {Observable} from 'rxjs/Observable';
import 'rxjs-compat/add/operator/do';

@Injectable()
export class NotAuthorizedUserGuard implements CanActivate {

  constructor(private userDetailsService: UserDetailsService,
              private router: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.userDetailsService.getCurrentUserUsername().map(username => {
      if( username === null ) {
        this.router.navigate(['login'], { queryParams: { returnUrl: state.url }});
      }
      return username !== null;
    });
  }
}
