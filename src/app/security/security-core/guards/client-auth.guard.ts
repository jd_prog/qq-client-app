import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Oauth2Service} from 'qq-core-lib';
import {Observable} from 'rxjs/Observable';
import 'rxjs-compat/add/operator/do';
import {AuthService} from 'angular5-social-login';

@Injectable()
export class ClientAuthGuard implements CanActivate {

  constructor(private authService: Oauth2Service,
              private socialAuthService: AuthService,
              private router: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.authService.isAuthenticated()
      .do(authenticated => {
        if (!authenticated) {
          this.router.navigate(['login']);
        }
      });

  }
}
