import {NgModule} from '@angular/core';
import {MaterialModule} from '../material/material.module';
import {LoginClientComponent} from './login-client/login-client.component';
import {SecurityCoreModule} from './security-core/security-core.module';
import {SecurityRoutingModule} from './security-routing.module';
import {ClientAuthGuard} from 'qq-core-lib';
import {FormsModule} from '@angular/forms';
import {environment} from '../../environments/environment';
import {NotAuthorizedUserGuard} from './security-core/guards/not-authorized-user.guard';

@NgModule({
  imports: [
    SecurityCoreModule,
    MaterialModule,
    SecurityRoutingModule,
    FormsModule
  ],
  declarations: [
    LoginClientComponent,
  ],
  providers: [
    ClientAuthGuard,
    NotAuthorizedUserGuard,
    { provide: 'apiKey', useValue: environment.linkedId.client_id }
  ],
  exports: [
    LoginClientComponent
  ]
})
export class SecurityModule {}
