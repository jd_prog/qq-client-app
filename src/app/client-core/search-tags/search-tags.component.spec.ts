import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SearchTagsComponent} from './search-tags.component';
import {MaterialModule} from '../../material/material.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FlexLayoutModule} from '@angular/flex-layout';
import {Event as Event2, EventHeaderService, EventsService, SearchEventSubject, StatusEvent, User} from 'qq-core-lib';
import {CUSTOM_ELEMENTS_SCHEMA, DebugElement, ElementRef, NO_ERRORS_SCHEMA, ViewChild} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {BrowserModule, By} from '@angular/platform-browser';
import {MatFormFieldModule} from '@angular/material';
import {CommonModule} from '@angular/common';
import {BrowserDynamicTestingModule} from '@angular/platform-browser-dynamic/testing';
import {BrowserTestingModule} from '@angular/platform-browser/testing';
import 'rxjs/Observable';
import {Observable} from 'rxjs';
import 'rxjs-compat/add/observable/of';

class MockSearchEventSubject {
  notify() {
  }
}

class MockEvent {
  preventDefault() {
  }
}

class MockEventsService {
  findPublished() {
  }

  getEventByTagNameIn(tag: string) {
  }
}

class MockEventHeaderService {
  sendEvents() {
  }
}

describe('SearchTagsComponent', () => {
  let componentFixture: ComponentFixture<SearchTagsComponent>;
  let component: SearchTagsComponent;
  let eventsService: EventsService;
  let eventHeaderService: EventHeaderService;
  let searchEventSubject: SearchEventSubject;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        MatFormFieldModule,
        BrowserTestingModule,
        BrowserDynamicTestingModule,
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        FlexLayoutModule
      ],
      declarations: [SearchTagsComponent],
      providers: [
        {provide: EventsService, useClass: MockEventsService},
        {provide: EventHeaderService, useClass: MockEventHeaderService},
        {provide: SearchEventSubject, useClass: MockSearchEventSubject},
        {provide: ElementRef, useValue: {}},
        {provide: Event, useClass: MockEvent},
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    componentFixture = TestBed.createComponent(SearchTagsComponent);
    component = componentFixture.componentInstance;
    componentFixture.detectChanges();
  });
  it('should start change method', () => {
    spyOn(component, 'send');
    component.change();
    setTimeout(() => {
      expect(component.send).toHaveBeenCalled();
    }, 200);
  });
  it('should start searchOff', () => {
    let count = 0;
    searchEventSubject = TestBed.get(SearchEventSubject);
    spyOn(searchEventSubject, 'notify').and.callFake(() => {
      count++;
    });
    component.searchOff();
    component.tag = 'fakeTag';
    component.searchOff();
    expect(count).toEqual(1);
  });
  it('should start backArrow', () => {
    searchEventSubject = TestBed.get(SearchEventSubject);
    spyOn(component, 'clear');
    spyOn(searchEventSubject, 'notify');
    component.backArrow();
    expect(searchEventSubject.notify).toHaveBeenCalled();
    expect(component.clear).toHaveBeenCalled();
  });
  it('should start clearOn', () => {
    let count = 0;
    let event: Event;
    event = undefined;
    spyOn(component, 'clear').and.callFake(() => {
      count++;
    });
    component.clearOn(event);
    event = TestBed.get(Event);
    spyOn(event, 'preventDefault').and.callFake(() => {
      count++;
    });

    component.clearOn(event);
    expect(count).toEqual(2);
  });
  it('should start clear', () => {
    const fakeCreator: User = {
      username: 'maxnelipa',
    };
    const fakeEventsList: Array<Event2> = [
      {
        id: 3,
        title: 'Java',
        description: '',
        location: null,
        dateStart: null,
        dateEnd: null,
        streams: null,
        status: StatusEvent.PUBLISH,
        participantsLimit: 0,
        creator: fakeCreator,
        tags: []
      },
      {
        id: 7,
        title: 'TypeScript',
        description: '',
        location: null,
        dateStart: null,
        dateEnd: null,
        streams: null,
        status: StatusEvent.PUBLISH,
        participantsLimit: 0,
        creator: fakeCreator,
        tags: []
      },
      {
        id: 9,
        title: 'JavaScript',
        description: '',
        location: null,
        dateStart: null,
        dateEnd: null,
        streams: null,
        status: StatusEvent.PUBLISH,
        participantsLimit: 0,
        creator: fakeCreator,
        tags: []
      },
      {
        id: 4,
        title: 'Kafka',
        description: '',
        location: null,
        dateStart: null,
        dateEnd: null,
        streams: null,
        status: StatusEvent.PUBLISH,
        participantsLimit: 0,
        creator: fakeCreator,
        tags: []
      },
      {
        id: 1,
        title: 'SQL',
        description: '',
        location: null,
        dateStart: null,
        dateEnd: null,
        streams: null,
        status: StatusEvent.PUBLISH,
        participantsLimit: 0,
        creator: fakeCreator,
        tags: []
      },
      {
        id: 90,
        title: 'Kotlin',
        description: '',
        location: null,
        dateStart: null,
        dateEnd: null,
        streams: null,
        status: StatusEvent.PUBLISH,
        participantsLimit: 0,
        creator: fakeCreator,
        tags: []
      }
    ];

    eventsService = TestBed.get(EventsService);
    eventHeaderService = TestBed.get(EventHeaderService);
    component.tag = 'fsdfdsg';
    spyOn(eventsService, 'findPublished').and.returnValue(Observable.of(fakeEventsList));
    spyOn(eventHeaderService, 'sendEvents');
    component.clear();
    expect(component.tag).toEqual('');
    expect(eventsService.findPublished).toHaveBeenCalled();
    expect(eventHeaderService.sendEvents).toHaveBeenCalledWith(fakeEventsList);
  });
  it('should start send with correct condition', () => {
    const fakeCreator: User = {
      username: 'maxnelipa',
    };
    const fakeEventsList: Array<Event2> = [
      {
        id: 3,
        title: 'Java',
        description: '',
        location: null,
        dateStart: null,
        dateEnd: null,
        streams: null,
        status: StatusEvent.PUBLISH,
        participantsLimit: 0,
        creator: fakeCreator,
        tags: []
      },
      {
        id: 7,
        title: 'TypeScript',
        description: '',
        location: null,
        dateStart: null,
        dateEnd: null,
        streams: null,
        status: StatusEvent.PUBLISH,
        participantsLimit: 0,
        creator: fakeCreator,
        tags: []
      },
      {
        id: 9,
        title: 'JavaScript',
        description: '',
        location: null,
        dateStart: null,
        dateEnd: null,
        streams: null,
        status: StatusEvent.PUBLISH,
        participantsLimit: 0,
        creator: fakeCreator,
        tags: []
      },
      {
        id: 4,
        title: 'Kafka',
        description: '',
        location: null,
        dateStart: null,
        dateEnd: null,
        streams: null,
        status: StatusEvent.PUBLISH,
        participantsLimit: 0,
        creator: fakeCreator,
        tags: []
      },
      {
        id: 1,
        title: 'SQL',
        description: '',
        location: null,
        dateStart: null,
        dateEnd: null,
        streams: null,
        status: StatusEvent.PUBLISH,
        participantsLimit: 0,
        creator: fakeCreator,
        tags: []
      },
      {
        id: 90,
        title: 'Kotlin',
        description: '',
        location: null,
        dateStart: null,
        dateEnd: null,
        streams: null,
        status: StatusEvent.PUBLISH,
        participantsLimit: 0,
        creator: fakeCreator,
        tags: []
      }
    ];
    component.tag = 'dsaf';
    eventsService = TestBed.get(EventsService);
    eventHeaderService = TestBed.get(EventHeaderService);
    spyOn(eventsService, 'getEventByTagNameIn').and.returnValue(Observable.of(fakeEventsList));
    spyOn(eventHeaderService, 'sendEvents');
    component.send();
    expect(eventsService.getEventByTagNameIn).toHaveBeenCalled();
    expect(eventHeaderService.sendEvents).toHaveBeenCalledWith(fakeEventsList);
  });
  it('should start send with else condition', () => {
    spyOn(component, 'clear');
    component.send();
    expect(component.clear).toHaveBeenCalled();
  });
});
