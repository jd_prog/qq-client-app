import {Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {EventHeaderService, EventsService, SearchEventSubject} from 'qq-core-lib';

@Component({
  selector: 'app-search-tags',
  templateUrl: './search-tags.component.html',
  styleUrls: ['./search-tags.component.less']
})
export class SearchTagsComponent implements OnInit {
  @ViewChild('input') inputEl: ElementRef;
  @ViewChild('mat') mat: ElementRef;
  tag: string;

  constructor(private eventsService: EventsService,
              private eventHeaderService: EventHeaderService,
              private  searchEventSubject: SearchEventSubject) {
  }

  ngOnInit(): void {
    this.inputEl.nativeElement.focus();
  }

  change(): void {
    setTimeout(() => {
      this.send();
    }, 200);
  }

  searchOff() {
    if (this.tag === undefined || this.tag.length === 0) {
      this.searchEventSubject.notify(false);
    }
  }

  backArrow() {
    this.clear();
    this.searchEventSubject.notify(false);
  }

  @HostListener('mousedown')
  clearOn(event: Event) {
    if (event) {
      event.preventDefault();
      this.clear();
    }
  }

  clear() {
    this.tag = '';
    this.eventsService.findPublished().subscribe(events => {
      this.eventHeaderService.sendEvents(events);
    });
  }

  send() {
    if (this.tag) {
      this.eventsService.getEventByTagNameIn(this.tag).subscribe(events => {
        this.eventHeaderService.sendEvents(events);
      });
    } else this.clear();
  }
}
