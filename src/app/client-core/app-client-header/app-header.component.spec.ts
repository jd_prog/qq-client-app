import {DebugElement} from '@angular/core';
import {AppHeaderComponent} from './app-header.component';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {BrowserTestingModule} from '@angular/platform-browser/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {routes} from '../../app-routing.module';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {EventPotentialVisitorsService, EventsService, User, UserDetailsService} from 'qq-core-lib';
import {e} from '@angular/core/src/render3';
import {By} from '@angular/platform-browser';
import {of} from 'rxjs';
import {MatChipsModule} from '@angular/material/chips';
import {MaterialModule} from '../../material/material.module';


describe('test of app header title', () => {

  let component: AppHeaderComponent;
  let componentFixture: ComponentFixture<AppHeaderComponent>;
  let de: DebugElement;

  let eventsService;
  let userDetailsService;
  let eventPotentialVisitorsService;


  beforeEach(async(() => {
    eventsService = jasmine.createSpyObj('EventsService', ['findAll']);
    userDetailsService = jasmine.createSpyObj('UserDetailsService', ['getCurrentUserUsername']);
    eventPotentialVisitorsService = jasmine.createSpyObj('EventPotentialVisitorsService', ['addEventPotentialVisitor', 'removeEventPotentialVisitor']);

    TestBed.configureTestingModule({
      declarations: [AppHeaderComponent],
      imports: [BrowserTestingModule,
        RouterTestingModule.withRoutes(routes),
        HttpClientTestingModule,
        MatChipsModule,
        MaterialModule
      ],
      providers: [
        {provide: EventsService, useValue: eventsService},
        {provide: UserDetailsService, useValue: userDetailsService},
        {provide: EventPotentialVisitorsService, useValue: eventPotentialVisitorsService}]
    }).compileComponents();
  }));

  beforeEach(() => {
    componentFixture = TestBed.createComponent(AppHeaderComponent);
    component = componentFixture.componentInstance;
    de = componentFixture.debugElement;
    componentFixture.detectChanges();
  });

  it('should have been created', () => {
    expect(component).toBeTruthy();
  });

  it('should have `title`', () => {
    component.title = 'title';
    expect(component.title).toBe('title');
  });


  it('should contain title in the html', () => {
    component.title = 'title';
    componentFixture.detectChanges();
    expect(de.query(By.css('h3')).nativeElement.innerText).toContain('title');
  });

  it('should toogle event potential visitors ', () => {
    const user = new User();
    const username = 'currentusername';
    userDetailsService.getCurrentUserUsername.and.returnValue(of(username));
    user.username = username;
    component.joinedToEvent = !component.joinedToEvent;
    expect(user.username).toContain(username);
    const addedOrRemovedUser = new User();
    addedOrRemovedUser.username = username;
    (component.joinedToEvent ? eventPotentialVisitorsService.addEventPotentialVisitor.and.returnValue(of(user))
      : eventPotentialVisitorsService.removeEventPotentialVisitor.and.returnValue(of(user)));
    expect(user.username).toEqual(addedOrRemovedUser.username);
  });


  it('should join to event', () => {
    const user = new User();
    const username = 'currentusername';
    component.joinedToEvent = false;
    userDetailsService.getCurrentUserUsername.and.returnValue(of(username));
    user.username = username;

    expect(user.username).toContain(username);
    const addedOrRemovedUser = new User();
    addedOrRemovedUser.username = username;
    component.joinedToEvent = !component.joinedToEvent;
    (component.joinedToEvent ? eventPotentialVisitorsService.addEventPotentialVisitor.and.returnValue(of(addedOrRemovedUser))
      : eventPotentialVisitorsService.removeEventPotentialVisitor.and.returnValue(of(addedOrRemovedUser)));

    expect(user.username).toEqual(addedOrRemovedUser.username);
  });


  it('should leave event', () => {
    const user = new User();
    const username = 'currentusername';
    component.joinedToEvent = true;
    userDetailsService.getCurrentUserUsername.and.returnValue(of(username));
    user.username = username;

    expect(user.username).toContain(username);
    const addedOrRemovedUser = new User();
    addedOrRemovedUser.username = username;
    component.joinedToEvent = !component.joinedToEvent;
    (component.joinedToEvent ? eventPotentialVisitorsService.addEventPotentialVisitor.and.returnValue(of(addedOrRemovedUser))
      : eventPotentialVisitorsService.removeEventPotentialVisitor.and.returnValue(of(addedOrRemovedUser)));

    expect(user.username).toEqual(addedOrRemovedUser.username);
  });

  it('should not display on event route', () => {
    expect(component.onEventRote('/events')).toEqual(false);
    expect(component.onEventRote('/events/upcoming')).toEqual(false);
    expect(component.onEventRote('/events/passed')).toEqual(false);
  });

  it('should return true because we are not on events list routes', () => {
    expect(component.onEventRote('/events/1/details')).toEqual(true);
  });


});
