import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {
  Event,
  EventPotentialVisitorsService,
  EventsService,
  EventStatusService,
  User,
  UserDetailsService,
  UserJoinNotificationService
} from 'qq-core-lib';
import {Observable, Subscription} from 'rxjs';
import 'rxjs/add/observable/zip';
import {MatChipInputEvent} from '@angular/material';
import {SearchEventSubject} from 'qq-core-lib';

/**
 * Created by yana on 23.04.18.
 */
@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.less']
})
export class AppHeaderComponent implements OnInit, OnDestroy {
  @Input() title: string;
  @Input() backRoute: string;
  public joinedToEvent: boolean;
  private joinEventSubscription: Subscription;
  tagsInputVisible = false;
  passed: boolean;
  event: Event;
  user: string;
  tags = [];
  tag: string;
  transRevers = false;
  appearance = false;

  constructor(private activatedRoute: ActivatedRoute,
              public router: Router,
              private eventsService: EventsService,
              private evensStatusService: EventStatusService,
              private userDetailsService: UserDetailsService,
              private eventPotentialVisitorsService: EventPotentialVisitorsService,
              private userJoinNotificationService: UserJoinNotificationService,
              private  searchEventSubject: SearchEventSubject) {
  }

  ngOnInit(): void {
    this.searchEventSubject.searchLifecycle.subscribe(it => {
      this.transRevers = !it;
      setTimeout(() => {
        this.tagsInputVisible = it;
        if (this.transRevers) {
          this.transRevers = false;
        }
      }, 450);
      this.appearance = true;

    });
    if (this.onEventRote(this.router.url)) {
      this.activatedRoute.params.map(params => params['eventId'])
        .flatMap(eventId => this.eventsService.findById(eventId))
        .subscribe(event => {
          this.event = event;
          this.passed = this.evensStatusService.passed(this.event);
          Observable.zip(this.eventPotentialVisitorsService.getAllPotentialVisitors(this.event.id), this.userDetailsService.getCurrentUserUsername())
            .subscribe(([potentialVisitors, username]) => {
              this.joinedToEvent = !!potentialVisitors.find(u => u.username === username);
            });
        });
    }
  }

  searchOn() {
    this.tagsInputVisible = true;
    this.searchEventSubject.notify(this.tagsInputVisible);
  }

  ngOnDestroy(): void {
    if (this.joinEventSubscription) {
      this.joinEventSubscription.unsubscribe();
    }
  }

  onEventRote(currentRouterUrl: string) {
    return currentRouterUrl !== '/events' && currentRouterUrl !== '/events/upcoming' && currentRouterUrl !== '/events/passed';
  }


  arrowBack() {
    this.router.navigate([this.backRoute], {relativeTo: this.activatedRoute});
  }

  toggleParticipation() {
    const user = new User();
    this.userDetailsService.getCurrentUserUsername().subscribe(username => {
      user.username = username;
      if (username === null) {
        this.router.navigate(['login'], {replaceUrl: true, queryParams: {returnUrl: this.router.url}});
      } else {
        this.joinedToEvent = !this.joinedToEvent;
        this.joinEventSubscription =
          (this.joinedToEvent ? this.eventPotentialVisitorsService.addEventPotentialVisitor(this.event.id, user) : this.eventPotentialVisitorsService.removeEventPotentialVisitor(this.event.id, user))
            .subscribe();
        this.joinedToEvent ? this.userJoinNotificationService.join() : this.userJoinNotificationService.leave();
      }
    });
  }


  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    if ((value || '').trim()) {
      this.tags.push(value.trim());
    }
    if (input) {
      input.value = '';
    }
  }
}
