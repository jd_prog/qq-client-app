import {NgModule} from '@angular/core';
import {AppHeaderComponent} from './app-client-header/app-header.component';
import {MaterialModule} from '../material/material.module';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {MatChipsModule} from '@angular/material/chips';
import {SearchTagsComponent} from './search-tags/search-tags.component';

@NgModule({
  imports: [
    MaterialModule,
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    MatChipsModule,
    FormsModule
  ],
  declarations: [
    AppHeaderComponent,
    SearchTagsComponent
  ],
  exports: [
    AppHeaderComponent
  ]
})
export class ClientCoreModule {
}
