import {BrowserModule} from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {EventModule} from './event/event.module';
import {SpeakerModule} from './speaker/speaker.module';
import {StreamModule} from './stream/stream.module';
import {TopicModule} from './topic/topic.module';
import {QuestionModule} from './question/question.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SecurityModule} from './security/security.module';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {environment} from '../environments/environment';
import {AngularFireModule} from 'angularfire2';
import {ServiceWorkerModule} from '@angular/service-worker';
import {QQRestConfig} from 'qq-core-lib';
import {ErrorModule} from "./error/error.module";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'qq-client-app'}),
    EventModule,
    SpeakerModule,
    StreamModule,
    TopicModule,
    SecurityModule,
    QuestionModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    environment.production ? ServiceWorkerModule.register('/ngsw-worker.js') : [],
    ErrorModule
  ],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
    QQRestConfig.baseUrl(environment.serverUrl);
  }
}
