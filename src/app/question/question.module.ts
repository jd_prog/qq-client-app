import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MaterialModule} from '../material/material.module';
import {
  QuestionAnsweredListItemComponent,
  QuestionDetailsComponent,
  QuestionOpenedListItemComponent,
  QuestionUpcomingListItemComponent,
  QuestionsComponent,
  QuestionAskComponent,
  QuestionsClientAnsweredListComponent,
  QuestionsClientUpcomingListComponent
} from './index';
import {ClientCoreModule} from '../client-core/client-core.module';
import {QuestionRoutingModule} from './question-routing.module';
import {SecurityModule} from '../security/security.module';
import {FormsModule} from '@angular/forms';
import {NgArrayPipesModule} from 'ngx-pipes';
import {ComponentsModule, ConfirmDialogWindowComponent, PipesModule} from 'qq-core-lib';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ClientCoreModule,
    QuestionRoutingModule,
    SecurityModule,
    NgArrayPipesModule,
    PipesModule,
    BrowserAnimationsModule,
    ComponentsModule
  ],
  declarations: [
    QuestionAnsweredListItemComponent,
    QuestionDetailsComponent,
    QuestionOpenedListItemComponent,
    QuestionUpcomingListItemComponent,
    QuestionsComponent,
    QuestionAskComponent,
    QuestionsClientAnsweredListComponent,
    QuestionsClientUpcomingListComponent
  ],
  exports: [
    QuestionAnsweredListItemComponent,
    QuestionDetailsComponent,
    QuestionOpenedListItemComponent,
    QuestionUpcomingListItemComponent,
    QuestionsComponent,
    QuestionAskComponent,
    QuestionsClientAnsweredListComponent,
    QuestionsClientUpcomingListComponent
  ],
  entryComponents: [
    ConfirmDialogWindowComponent
  ],
})
export class QuestionModule { }
