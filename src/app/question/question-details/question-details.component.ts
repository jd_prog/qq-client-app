/**
 * Created by yana on 27.04.18.
 */
import {Component, OnDestroy, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute, Params, Router} from '@angular/router';

import {
  QuestionsService,
  Question,
  Speaker,
  Topic,
  UserDetailsService,
  TopicService,
  SpeakersService,
  TopicStatusService, ConfirmDialogWindowComponent
} from 'qq-core-lib';

import {QuestionComponent} from '../question.component';

import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';

import 'rxjs-compat/add/operator/mergeMap';
import {map, tap} from 'rxjs/operators';
import {merge} from 'rxjs/internal/observable/merge';
import {combineLatest} from 'rxjs';
import {MatDialog} from '@angular/material';
import 'rxjs-compat/add/observable/empty';

@Component({
  selector: 'app-question-details',
  templateUrl: './question-details.component.html',
  styleUrls: ['./question-details.component.less']
})
export class QuestionDetailsComponent extends QuestionComponent implements OnInit, OnDestroy {

  isTopicPassed: boolean;

  private questionSubscription: Subscription;
  private votesSubscription: Subscription;

  private usernameObservable: Observable<string>;
  private alertDialogSubscription: Subscription;

  constructor(protected activatedRoute: ActivatedRoute,
              protected router: Router,
              protected userDetailsService: UserDetailsService,
              private location: Location,
              private questionsService: QuestionsService,
              private speakersService: SpeakersService,
              private topicService: TopicService,
              private topicStatusService: TopicStatusService,
              private dialog: MatDialog) {
    super(activatedRoute, router, userDetailsService);
  }

  ngOnInit(): void {

    this.usernameObservable = this.loadUsername();

    this.questionSubscription = merge(
      this.checkIfVotedOrCreatedByUser(this.usernameObservable, this.loadQuestion()),
      this.checkIfCanModerate(this.usernameObservable, this.loadSpeakers()),
      this.checkIfTopicPassed())
      .subscribe();
  }

  ngOnDestroy(): void {
    this.questionSubscription.unsubscribe();
    if (this.votesSubscription) {
      this.votesSubscription.unsubscribe();
    }
  }

  private checkIfCanModerate(usernameObservable: Observable<string>,
                             speakersObservable: Observable<Speaker[]>): Observable<void> {

    return combineLatest(usernameObservable, speakersObservable)
      .pipe(map(results => {
        const username: string = results[0];
        const speakers: Speaker[] = results[1];

        this.questionMetadata.canModerate = this.canBeModeratedByUser(username, speakers);
      }));
  }

  private loadQuestion(): Observable<Question> {
    return this.activatedRoute.params
      .map(params => params['questionId'])
      .flatMap(questionId => this.questionsService.findById(questionId));
  }

  private checkIfTopicPassed(): Observable<Topic> {

    return this.activatedRoute.params
      .map(params => params['topicId'])
      .flatMap(topicId => this.topicService.findById(topicId))
      .pipe(tap(topic => this.isTopicPassed = this.topicStatusService.passed(topic)));
  }

  private loadSpeakers(): Observable<Speaker[]> {
    return this.activatedRoute.params
      .map(params => params['topicId'])
      .flatMap(topicId => this.speakersService.findAllByTopic(topicId));
  }

  private canBeModeratedByUser(username: string, speakers: Speaker[]): boolean {
    return speakers.find(speaker => speaker.username === username) !== undefined;
  }

  public voteForQuestion() {

    const questionObservable =
      (this.questionMetadata.votedByCurrentUser
        ? this.questionsService.removeVote(this.question.id)
        : this.questionsService.vote(this.question.id));

    this.votesSubscription = this.checkIfVotedOrCreatedByUser(this.usernameObservable, questionObservable).subscribe();
  }

  deleteQuestion() {

    this.showConfirmationAlert(
      'Remove this question?',
      this.questionsService.delete(this.question.id)
        .flatMap(() => this.activatedRoute.params)
        .pipe(tap(params => this.navigateToQuestionsList(params['eventId'], params['id'], params['topicId']))));
  }




  startAnswering(): Observable<Question> {

    return this.questionsService.open(this.question.id)
      .pipe(tap(question => this.question = question));
  }

  closeQuestion(): Observable<Question> {
    return this.questionsService.close(this.question.id)
      .pipe(tap(question => this.question = question));
  }

  private navigateToQuestionsList(eventId: string, streamId: string, topicId: string) {
    this.router.navigate([
        'events', eventId,
        'streams', streamId,
        'topic', topicId,
        'questions', 'upcoming'],
      {replaceUrl: true});
  }

  private showConfirmationAlert(textMessage: string, onPositiveBtnClicked: Observable<void | Params>) {

    this.alertDialogSubscription = this.dialog.open(ConfirmDialogWindowComponent,
      {
        data: {
          text: textMessage,
          entity: {title: ` \"${this.question.content}\" `},
          positiveAnswer: 'Yes',
          negativeAnswer: 'No'
        }
      })
      .afterClosed()
      .flatMap(confirmed => confirmed ? onPositiveBtnClicked : Observable.empty())
      .subscribe();
  }



}
