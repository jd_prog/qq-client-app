/**
 * Created by yana on 26.04.18.
 */
import {OnDestroy, OnInit} from '@angular/core';
import {Question, QuestionsService} from 'qq-core-lib';
import {Subscription} from 'rxjs/Subscription';

export class QuestionsListComponent implements OnInit, OnDestroy {

  questions: Question[];

  private questionsSubscription: Subscription;

  constructor(private questionsService: QuestionsService) {
  }

  ngOnInit() {
    this.subscribeOnQuestions();
  }

  ngOnDestroy() {
    this.questionsSubscription.unsubscribe();
  }

  private subscribeOnQuestions() {
    this.questionsSubscription = this.questionsService.questionsCollection
      .subscribe(questions => this.questions = questions);
  }
}
