import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {Question, UserDetailsService, QuestionsService} from 'qq-core-lib';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, Validators} from '@angular/forms';
import {ErrorStateMatcher, ShowOnDirtyErrorStateMatcher} from '@angular/material';

/**
 * Created by yana on 27.04.18.
 */

@Component({
  selector: 'app-question-ask',
  templateUrl: './question-ask.component.html',
  styleUrls: ['./question-ask.component.less'],
  providers: [ {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}]
})
export class QuestionAskComponent implements OnInit, OnDestroy {

  question = new Question();
  questionFormControl = new FormControl(this.question.content, [
    Validators.required,
    Validators.pattern('(\\n|.)*(^(?!\\s*$).+)(\\n|.)*')
  ]);



  userProfileName: string;

  private topicId: string;

  private questionSubscription?: Subscription;
  private routeParamsSubscription: Subscription;
  private profileSubscription: Subscription;

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private userDetailsService: UserDetailsService,
              private questionsService: QuestionsService) {
  }

  ngOnInit(): void {
    this.requestUserProfileName();
    this.parseRouteParams();
  }



  ngOnDestroy(): void {
    this.routeParamsSubscription.unsubscribe();
    this.profileSubscription.unsubscribe();
    if (this.questionSubscription) {
      this.questionSubscription.unsubscribe();
    }
  }


  sendQuestion() {

    this.questionSubscription = this.questionsService.post(this.topicId, this.question)
      .subscribe(question => this.openQuestionDetails(question.id));

  }

  private parseRouteParams() {
    this.routeParamsSubscription = this.activatedRoute.params
      .subscribe(params => this.topicId = params['topicId']);
  }

  private requestUserProfileName() {

    this.profileSubscription = this.userDetailsService.getCurrentUserDisplayName()
      .subscribe(profileName => this.userProfileName = profileName);
  }

  private openQuestionDetails(questionId: number) {
    this.router.navigateByUrl(this.router.url.replace('ask', questionId.toString()), {replaceUrl: true});
  }
}
