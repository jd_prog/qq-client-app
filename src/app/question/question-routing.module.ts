import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {
  QuestionAskComponent,
  QuestionDetailsComponent,
  QuestionsClientAnsweredListComponent,
  QuestionsClientUpcomingListComponent,
  QuestionsComponent
} from './index';
import {ClientAuthGuard} from 'qq-core-lib';
import {PingBackEndGuard} from '../event/event-resolver/ping-back-end-guard.service';
import {NotAuthorizedUserGuard} from '../security/security-core/guards/not-authorized-user.guard';

const routes: Routes = [
  {
    path: 'events/:eventId/streams/:id/topic/:topicId/questions',
    component: QuestionsComponent,
    canActivate: [PingBackEndGuard , NotAuthorizedUserGuard],
    children: [
      {path: 'upcoming', component: QuestionsClientUpcomingListComponent , canActivate: [NotAuthorizedUserGuard]},
      {path: 'answered', component: QuestionsClientAnsweredListComponent , canActivate: [NotAuthorizedUserGuard]}
    ]
  },
  {path: 'events/:eventId/streams/:id/topic/:topicId/questions/ask', component: QuestionAskComponent, canActivate: [PingBackEndGuard, NotAuthorizedUserGuard]},
  {path: 'events/:eventId/streams/:id/topic/:topicId/questions/:questionId', component: QuestionDetailsComponent, canActivate: [PingBackEndGuard, NotAuthorizedUserGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuestionRoutingModule {
}
