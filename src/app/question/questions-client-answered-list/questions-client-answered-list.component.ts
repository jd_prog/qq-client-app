import {Component} from '@angular/core';
import {QuestionsListComponent} from '../questions-list/questions-list.component';
import {QuestionsService} from 'qq-core-lib';

@Component({
  selector: 'app-questions-client-answered-list',
  templateUrl: './questions-client-answered-list.component.html',
  styleUrls: ['./questions-client-answered-list.component.less']
})
export class QuestionsClientAnsweredListComponent extends QuestionsListComponent {

  constructor(questionsService: QuestionsService) {
    super(questionsService);
  }
}
