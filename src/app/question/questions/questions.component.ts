/**
 * Created by yana on 20.04.18.
 */
import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Question, QuestionsService, TopicService, TopicStatusService} from 'qq-core-lib';
import {Subscription} from 'rxjs';
import {tap} from 'rxjs/operators';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.less']
})
export class QuestionsComponent implements OnInit, OnDestroy {

  public questions: Array<Question>;
  public isTopicPassed: boolean;
  private questionsSubscription: Subscription;

  tabLinks = [
    {
      'path': 'upcoming',
      'label': 'Upcoming'
    },
    {
      'path': 'answered',
      'label': 'Answered'
    }];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private questionsService: QuestionsService,
              private topicService: TopicService,
              private topicStatusService: TopicStatusService) {
  }

  ngOnInit() {
    this.questionsSubscription =
      this.route.params.map(params => params['topicId'])
        .flatMap(topicId => this.topicService.findById(topicId))
        .pipe(tap(topic => this.isTopicPassed = this.topicStatusService.passed(topic)))
        .flatMap(topic => this.questionsService.findAllByTopicId(topic.id))
        .subscribe(questions => this.questions = questions);
  }

  ngOnDestroy() {
    this.questionsSubscription.unsubscribe();
  }

  public openAskQuestionForm() {
    this.router.navigate(['ask'], {relativeTo: this.route});
  }

}
