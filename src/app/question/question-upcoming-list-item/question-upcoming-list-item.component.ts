/**
 * Created by yana on 26.04.18.
 */
import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserDetailsService} from 'qq-core-lib';
import {QuestionListItemComponent} from '../question-list-tem.component';

@Component({
  selector: 'app-question-upcoming-list-item',
  templateUrl: './question-upcoming-list-item.component.html',
  styleUrls: ['./question-upcoming-list-item.component.less']
})
export class QuestionUpcomingListItemComponent extends QuestionListItemComponent {

  constructor(activatedRoute: ActivatedRoute,
              router: Router,
              userDetailsService: UserDetailsService) {
    super(activatedRoute, router, userDetailsService);
  }
}
