import {Component, OnInit} from '@angular/core';
import {QuestionsListComponent} from '../questions-list/questions-list.component';
import {ActivatedRoute} from '@angular/router';
import {TopicService, Question, QuestionsService, TopicStatusService, QuestionStatusService} from 'qq-core-lib';

@Component({
  selector: 'app-questions-client-upcoming-list',
  templateUrl: './questions-client-upcoming-list.component.html',
  styleUrls: ['./questions-client-upcoming-list.component.less']
})
export class QuestionsClientUpcomingListComponent extends QuestionsListComponent implements OnInit {

  isTopicPassed: boolean;

  constructor(questionsService: QuestionsService,
              private topicService: TopicService,
              private topicStatusService: TopicStatusService,
              private questionStatusService: QuestionStatusService,
              private route: ActivatedRoute) {
    super(questionsService);
  }

  ngOnInit() {
    super.ngOnInit();

    this.route.params.map(params => params['topicId'])
      .flatMap(topicId => this.topicService.findById(topicId))
      .subscribe(topic => this.isTopicPassed = this.topicStatusService.passed(topic));
  }

  isUpcoming = (question: Question) => {
    return this.questionStatusService.isUpcoming(this.isTopicPassed, question);
  }


}
