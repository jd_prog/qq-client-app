import {Component} from '@angular/core';
import {QuestionComponent} from '../question.component';
import {ActivatedRoute, Router} from '@angular/router';
import {UserDetailsService} from 'qq-core-lib';

@Component({
  selector: 'app-question-opened-list-item',
  templateUrl: './question-opened-list-item.component.html',
  styleUrls: ['./question-opened-list-item.component.less']
})
export class QuestionOpenedListItemComponent extends QuestionComponent {



  constructor(activatedRoute: ActivatedRoute,
              router: Router,
              userDetailsService: UserDetailsService) {
    super(activatedRoute, router, userDetailsService);

  }
}
