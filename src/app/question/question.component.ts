import {Input} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {Question, UserDetailsService, Vote} from 'qq-core-lib';
import {Observable} from 'rxjs/Observable';
import {map} from 'rxjs/operators';
import {combineLatest} from 'rxjs/index';

/**
 * Created by yana on 27.04.18.
 */

export class QuestionComponent {

  @Input('question')
  public question: Question;

  questionMetadata = {
    'canModerate': false,
    'votedByCurrentUser': false,
    'createdByCurrentUser': false
  };

  constructor(protected activatedRoute: ActivatedRoute,
              protected router: Router,
              protected userDetailsService: UserDetailsService) {
  }

  public getVotesPluralValue(): string {
    return this.question.votes.length > 1 ? 'few' : this.question.votes.length.toString();
  }

  public openDetails() {

    this.activatedRoute.params.subscribe(params => {

      this.router.navigate(
        ['events', params['eventId'],
          'streams', params['id'],
          'topic', params['topicId'],
          'questions', this.question.id]);
    });
  }

  protected checkIfVotedOrCreatedByUser(usernameObservable: Observable<string>,
                                      questionObservable: Observable<Question>): Observable<void> {

    return combineLatest(usernameObservable, questionObservable)
      .pipe(map(results => {
        const username: string = results[0];
        this.question = results[1];

        this.questionMetadata.createdByCurrentUser = this.isCreatedByUser(username, this.question);
        this.questionMetadata.votedByCurrentUser = this.isVotedByUser(username, this.question.votes);
      }));
  }

  protected isVotedByUser(username: string, votes: Vote[]): boolean {
    const userVote = votes.find(vote => vote.username === username);
    return userVote && userVote !== null;
  }

  protected isCreatedByUser(username: string, question: Question): boolean {
    return question.authorUserName === username;
  }

  protected loadUsername(): Observable<string> {
    return this.userDetailsService.getCurrentUserUsername();
  }

  public isAnswered(): boolean {
    if(this.question !== undefined) {
      return this.question.questionStatus === 'CLOSED';
    } return true;
  }
}
