import {QuestionComponent} from './question.component';
import {Observable} from 'rxjs/Observable';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {UserDetailsService} from 'qq-core-lib';
import {OnDestroy, OnInit} from '@angular/core';
import { empty, of } from 'rxjs';

export class QuestionListItemComponent extends QuestionComponent implements OnInit, OnDestroy {

  private questionSubscription: Subscription;

  constructor(activatedRoute: ActivatedRoute,
              router: Router,
              userDetailsService: UserDetailsService) {
    super(activatedRoute, router, userDetailsService);
  }

  ngOnInit() {
    this.questionSubscription = this.checkIfVotedOrCreatedByUser(this.loadUsername(), of(this.question)).subscribe();
  }

  ngOnDestroy(): void {
    this.questionSubscription.unsubscribe();
  }
}
