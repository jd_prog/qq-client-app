import localeEn from '@angular/common/locales/en';
import {registerLocaleData} from '@angular/common';
import localeEnExtra from '@angular/common/locales/extra/en';
import localeUk from '@angular/common/locales/uk';
import localeUkExtra from '@angular/common/locales/extra/uk';

declare const require;

let language = 'en';

const languages = ['en', 'uk'];

languages.forEach(it => {
  if (navigator.languages.includes(it)) {
    language = it;
  }
});


registerLocaleData(localeEn, 'en' , localeEnExtra);

const localesMap = [{'locale': 'en', 'localeMethod': localeEn, 'localeExtra': localeEnExtra}, {'locale': 'uk', 'localeMethod' : localeUk, 'localeExtra': localeUkExtra}];
localesMap.forEach( it => {
  if ( language === it.locale ) {
    registerLocaleData(it.localeMethod, it.locale, it.localeExtra);
  };
});


export const translations = require(`raw-loader!./locale/messages.${language}.xlf`);
