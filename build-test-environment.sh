#!/usr/bin/env bash

timestamp=$(date "+%s")
echo "$timestamp"
docker_build="docker build -t vitech.com.ua:5001/qq/qqclientapp-$BITBUCKET_BRANCH -f Dockerfile.test ."
docker_tag="docker tag vitech.com.ua:5001/qq/qqclientapp-$BITBUCKET_BRANCH vitech.com.ua:5001/qq/qqclientapp-$BITBUCKET_BRANCH:$timestamp"
docker_push="docker push vitech.com.ua:5001/qq/qqclientapp-$BITBUCKET_BRANCH"
eval $docker_build
eval $docker_tag
eval $docker_push
